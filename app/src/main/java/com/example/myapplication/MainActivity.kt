package com.example.myapplication

import android.bluetooth.BluetoothAdapter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import okhttp3.*
import java.util.Timer
import kotlin.concurrent.schedule
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import android.content.Intent

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun attend(v: View){

        val pressedCheck: TextView = findViewById(R.id.pressedCheck)

        val bta: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        val discoverableIntent: Intent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE).apply {
            putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
        }
        startActivity(discoverableIntent)


        pressedCheck.text = "DISCOVERY STARTED"

        Timer("WaitingForDiscoveryToEnd", false)
            .schedule(2000) {
                val client = OkHttpClient()

                val date = LocalDateTime.now()
                val formatter = DateTimeFormatter.ofPattern("d-M-yyyy")
                val dateString = date.format(formatter)


                val request = Request.Builder().url(
                    "https://firestore.googleapis.com/v1/projects/attendancelistapp/" +
                            "databases/(default)/documents/attendance/$dateString/students/201632422"
                ).build()

                val response:Response = client.newCall(request).execute()

                this@MainActivity.runOnUiThread{
                    if(response.code() == 200){
                        pressedCheck.text = "SUCCESSFULLY ATTENDED"
                    }
                    else{
                        pressedCheck.text = "ATTENDANCE FAILED TO REGISTER"
                    }
                    println(response.toString())
                }

                //bta.disable()
            }
    }
}
